(function ($) {

    "use strict";
    angular.module('app')
            .directive('a', A)
            .directive('mcuiFormInput', McuiFormInput)
            .directive('mcuiFormSelect', McuiFormSelect)
            .directive('mcuiBox', McuiBox)
            .directive('mcuiBoxFooterSlot', McuiBoxFooterSlot)
            .directive('mcuiBoxToolSlot', McuiBoxToolSlot)
            .directive('mcuiBtnSearch', McuiBtnSearch)
            .directive('mcuiBtnWidgetCollapse', McuiBtnWidgetCollapse)
            .directive('mcuiBtnWidgetRemove', McuiBtnWidgetRemove)
            .directive('mcuiBtnActionimage', McuiBtnActionimage);

    /*@ngInject*/
    function A() {
        return {
            restrict: 'E',
            scope: {linkDisabled: '=', linkBlocked: '='},
            link: function (scope, element, attrs) {

                if (scope.linkDisabled === true) {
                    $(element).addClass('link-disabled');
                }

                if (scope.linkBlocked === true) {
                    $(element).addClass('link-blocked');
                }

                if (attrs.ngClick || attrs.href === '' || attrs.href === '#' || /^#tab/.test(attrs.href) || /^#carousel/.test(attrs.href)) {
                    element.on('click', function (e) {
                        e.preventDefault();
                    });
                }
            }
        };
    }

    /*@ngInject*/
    function McuiFormInput() {

        var directive = {
            restrict: "EA",
            replace: true,
            require: 'ngModel',
            template: function () {
                var template = '';
                template += '<div class="form-group">';
                template += '<label>{{ label }}</label>';
                template += '<input type="text" class="form-control" ng-model="ngModel" />';
                template += '</div>';

                return template;
            },
            link: function (scope, element, attrs) {

                if (angular.isDefined(attrs.input)) {
                    var properties = scope.$eval(attrs.input);
                    for (var prop in properties)
                        element.find('.form-control').prop(prop, properties[prop]);
                }
            },
            scope: {
                ngModel: "=",
                label: "@",
                input: "@",
                type: "@"
            }
        };

        return directive;
    }

    /*@ngInject*/
    function McuiFormSelect() {

        var directive = {
            restrict: "EA",
            replace: true,
            require: 'ngModel',
            template: function () {

                var template = '<div class="form-group">' +
                        '                <label>{{ label }}</label>' +
                        '                <select class="form-control"' +
                        '                        ng-options="option.nombre for option in collection track by option.id"' +
                        '                        ng-model="ngModel">' +
                        '                </select>                ' +
                        '        </div>';


                return template;
            },
            link: function (scope, element, attrs) {

                if (angular.isDefined(attrs.properties)) {
                    var properties = scope.$eval(attrs.properties);
                    for (var prop in properties)
                        element.find('.form-control').prop(prop, properties[prop]);
                }
            },
            scope: {
                ngModel: "=",
                label: "@",
                properties: "@"
            }
        };

        return directive;
    }

    /*@ngInject*/
    function McuiBox() {

        var directive = {
            restrict: "EA",
            replace: true,
            transclude: {boxFooterSlot: '?mcuiBoxFooterSlot', boxToolSlot: '?mcuiBoxToolSlot'},
            template: function () {
                var template = '' +
                        '<div class="box" ng-class="boxClass">' +
                        '    <div class="box-header with-border" ng-class="boxHeaderClass"><h3 class="box-title">{{ boxTitle }}</h3>' +
                        '       <div class="box-tools pull-right" ng-transclude="boxToolSlot"></div>' +
                        '    </div>' +
                        '    <div class="box-body" ng-class="boxBodyClass"><ng-transclude></ng-transclude></div>' +
                        '    <div class="box-footer" ng-class="boxFooterClass" ng-if="!footerSlot" ng-transclude="boxFooterSlot"></div>' +
                        '</div>';
                return template;
            },
            link: function (scope, element, attrs) {
                scope.boxClass = attrs.boxClass || "box-primary";
                scope.boxFooterClass = attrs.boxFooterClass || "";
                scope.boxHeaderClass = attrs.boxHeaderClass || "";
            },
            scope: {
                boxTitle: "@",
                boxClass: "@",
                boxBodyClass: "@",
                boxFooterClass: "@",
                footerSlot: "@",
                boxHeaderClass: "@"
            }
        };

        return directive;
    }

    /*@ngInject*/
    function McuiBoxFooterSlot() {
        var directive = {
            restrict: "EA"
        };
        return directive;
    }

    /*@ngInject*/
    function McuiBoxToolSlot() {
        var directive = {
            restrict: "EA",
            replace: true
        };
        return directive;
    }

    /*@ngInject*/
    function McuiBtnSearch() {
        var directive = {
            restrict: "EA",
            replace: true,
            template: function () {
                return '<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>'
            }
        };
        return directive;
    }

    /*@ngInject*/
    function McuiBtnWidgetCollapse() {
        var directive = {
            restrict: "EA",
            replace: true,
            template: function () {
                return '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>';
            }
        };
        return directive;
    }

    /*@ngInject*/
    function McuiBtnWidgetRemove() {
        var directive = {
            restrict: "EA",
            replace: true,
            template: function () {
                return '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>';
            }
        };
        return directive;
    }

    /*@ngInject*/
    function McuiBtnActionimage() {
        return {
            restrict: 'EA',
            template: function () {

                var template = '<div class="btn-group btn-action-image">' +
                        '                                <button type="button" class="btn btn-primary flat">Acciones</button>' +
                        '                                <button type="button" class="btn btn-primary dropdown-toggle flat" data-toggle="dropdown" aria-expanded="true">' +
                        '                                    <span class="caret"></span>' +
                        '                                    <span class="sr-only">Toggle Dropdown</span>' +
                        '                                </button>' +
                        '                                <ul class="dropdown-menu" role="menu">' +
                        '                                    <li><a href="#" data-ng-click="executeDelete()">Eliminar</a></li>' +
                        '                                    <li><a href="#" data-ng-click="executeEdit()">Editar</a></li>' +
                        '                                </ul>' +
                        '                            </div>';

                return template;
            },
            link: function (scope, element, attrs) {

                var reference = scope.ref || null;

                scope.executeDelete = function () {
                    scope.deleteFn()(reference);
                };

                scope.executeEdit = function () {
                    scope.editFn()(reference);
                };
            },
            scope: {
                deleteFn: '&',
                editFn: '&',
                ref: '@'
            }
        }
    }

})(jQuery);    