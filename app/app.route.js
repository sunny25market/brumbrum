(function () {

    "use strict";

    angular.module('app')
            .config(ConfigRoute)
            .constant('APP_ROOT', {
                base: 'app/',
                views: 'app/views/',
                layouts: 'app/views/layouts/'
            })
            .constant('AUTH_ROOT', {
                base: 'app/module/auth/',
                views: 'app/module/auth/views/',
                layouts: 'app/module/auth/views/layouts/'
            })
            .constant('CAR_ROOT', {
                base: 'app/module/car/',
                views: 'app/module/car/views/',
                layouts: 'app/module/car/views/layouts/'
            })
            .constant('USER_ROOT', {
                base: 'app/module/user/',
                views: 'app/module/user/views/',
                layouts: 'app/module/user/views/layouts/'
            })
            .constant('MESSAGE_ROOT', {
                base: 'app/module/message/',
                views: 'app/module/message/views/',
                layouts: 'app/module/message/views/layouts/'
            })
            .constant('DRIVINGTEST_ROOT', {
                base: 'app/module/drivingtest/',
                views: 'app/module/drivingtest/views/',
                layouts: 'app/module/drivingtest/views/layouts/'
            });

    /*@ngInject*/
    function ConfigRoute($urlRouterProvider, $stateProvider, APP_ROOT, AUTH_ROOT, CAR_ROOT, USER_ROOT, MESSAGE_ROOT, DRIVINGTEST_ROOT) {

        $urlRouterProvider
                .when("/", "/auth/login")
                .when("", "/auth/login")
                .otherwise("/404");

        $stateProvider
                .state('site', {
                    abstract: true,
                    data: {},
                    views: {
                        '': {
                            templateUrl: APP_ROOT.layouts + '_main.html'
                        }
                    }
                })
                .state('init', {
                    abstract: true,
                    data: {},
                    views: {
                        '': {
                            templateUrl: APP_ROOT.layouts + '_init.html'
                        }
                    },
                    resolve: {
                    }
                })
                .state('site.layout', {
                    abstract: true,
                    data: {},
                    views: {
                        'header@site': {
                            templateUrl: APP_ROOT.views + 'components/cmp.header.html',
                        },
                        'leftpanel@site': {
                            templateUrl: APP_ROOT.views + 'components/cmp.leftpanel.html',
                            //controller: 'LeftPanelController as lpanel'
                        },
                        'footer@site': {
                            templateUrl: APP_ROOT.views + 'components/cmp.footer.html',
                            //controller: 'LeftPanelController as lpanel'
                        },
                        'controlsidebar@site': {
                            templateUrl: APP_ROOT.views + 'components/cmp.controlsidebar.html',
                            //controller: 'LeftPanelController as lpanel'
                        },
                        'pagecontentheader@site': {
                            templateUrl: APP_ROOT.views + 'components/cmp.pagecontentheader.html',
                            //controller: 'LeftPanelController as lpanel'
                        }
                    }
                })
                .state('init.layout', {
                    abstract: true,
                    data: {},
                    views: {}
                })
                .state('404', {
                    url: "/404",
                    parent: 'init.layout',
                    data: {pageTitle: "404"},
                    views: {
                        'content@init': {
                            templateUrl: APP_ROOT.views + 'partials/error/404.html'
                        }
                    }
                })
                .state('site.home', {
                    url: "/home",
                    parent: 'site.layout',
                    data: {
                        pageTitle: "Dashboard",
                        pageHeader: "Dashboard",
                        pageDescription: "Panel de Administración"},
                    views: {
                        'content@site': {
                            templateUrl: APP_ROOT.views + 'home.html',
                            controller: 'HomeController as vm'
                        }
                    }
                })
                //
                //AUTH MODULE
                //
                .state('auth', {
                    url: "/auth",
                    parent: 'init.layout',
                    abstract: true,
                    views: {
                        'content@init': {
                            templateUrl: AUTH_ROOT.layouts + 'main.html',
                            controller: 'AuthController'
                        }
                    }
                })
                .state('auth.login', {
                    url: "/login",
                    parent: 'auth',
                    data: {pageTitle: "Autenticar"},
                    views: {
                        '': {
                            templateUrl: AUTH_ROOT.views + 'login.html',
                            controller: 'LoginController as vm'
                        }
                    }
                })
                .state('auth.logout', {
                    url: "/logout",
                    parent: 'auth',
                    data: {pageTitle: "Cerrando..."},
                    views: {
                        '': {
                            templateUrl: AUTH_ROOT.views + 'logout.html',
                            controller: 'LogoutController as vm'
                        }
                    }
                })
                .state('auth.recover', {
                    url: "/recover",
                    parent: 'auth',
                    data: {pageTitle: "Recuperar contraseña"},
                    views: {
                        '': {
                            templateUrl: AUTH_ROOT.views + 'recover.html',
                            controller: 'RecoverController as vm'
                        }
                    }
                })
                .state('auth.reset', {
                    url: "/reset",
                    parent: 'auth',
                    data: {pageTitle: "Nueva contraseña..."},
                    views: {
                        '': {
                            templateUrl: AUTH_ROOT.views + 'reset.html',
                            controller: 'ResetController as vm'
                        }
                    }
                })
                //
                //CAR ROUTER
                //                
                .state('car', {
                    abstract: true,
                    parent: 'site.layout',
                    url: "/car",
                    data: {pageTitle: "Vehículos"},
                    views: {
                        'content@site': {
                            templateUrl: CAR_ROOT.layouts + "main.html"
                        }
                    }
                })
                /*.state('car.list', {
                 parent: 'car',
                 url: "/list",
                 data: {
                 pageTitle: "Automóviles",
                 pageHeader: "Listado de automóviles",
                 pageDescription: "Listado de automóviles. Inventario."
                 },
                 views: {
                 '': {
                 templateUrl: CAR_ROOT.views + "list.html",
                 controller: "CarListController as vm"
                 }
                 }
                 })*/
                .state('car.create', {
                    parent: 'car',
                    url: "/create",
                    data: {
                        pageTitle: "Nueva Vehículo",
                        pageHeader: "Agregar vehículo",
                        pageDescription: "Ingrese nuevo vehículo al stock."
                    },
                    views: {
                        '': {
                            templateUrl: CAR_ROOT.views + "create.html",
                            controller: "CarCreateController as vm"
                        }
                    }
                })
                /*.state('car.edit', {
                 parent: 'car',
                 url: "/edit/{id}",
                 data: {
                 pageTitle: "Editar Automóvil",
                 pageHeader: "Editar Automóvil",
                 pageDescription: "Formulario de edición de Automóvil"
                 },
                 views: {
                 '': {
                 templateUrl: CAR_ROOT.views + "edit.html",
                 controller: "CarEditController as vm"
                 }
                 }
                 })
                 .state('car.view', {
                 parent: 'car',
                 url: "/view/{id}",
                 data: {
                 pageTitle: "Detalle Automóvil",
                 pageHeader: "Detalle Automóvil",
                 pageDescription: "Detalle de Automóvil"
                 },
                 views: {
                 '': {
                 templateUrl: CAR_ROOT.views + "view.html",
                 controller: "CarViewController as vm"
                 }
                 }
                 })*/
                //
                //USER ROUTER
                //                
                .state('user', {
                    abstract: true,
                    parent: 'site.layout',
                    url: "/user",
                    data: {pageTitle: "Usuarios"},
                    views: {
                        'content@site': {
                            templateUrl: USER_ROOT.layouts + "main.html"
                        }
                    }
                })
                .state('user.profile', {
                    parent: 'user',
                    url: "/profile/{id}",
                    data: {
                        pageTitle: "Perfil de usuario",
                        pageHeader: "Perfil de usuario",
                        pageDescription: "Visualizando perfil de usuario."
                    },
                    views: {
                        '': {
                            templateUrl: USER_ROOT.views + "profile.view.html",
                            controller: "ProfileViewController as vm"
                        }
                    }
                })
                .state('user.profile_edit', {
                    parent: 'user',
                    url: "/profile/edit/{id}",
                    data: {
                        pageTitle: "Modificar Perfil de usuario",
                        pageHeader: "Modificar Perfil de usuario",
                        pageDescription: "Modificando el perfil de usuario."
                    },
                    views: {
                        '': {
                            templateUrl: USER_ROOT.views + "profile.edit.html",
                            controller: "ProfileEditController as vm"
                        }
                    }
                })
                //
                //MESSAGE ROUTER
                //                
                .state('message', {
                    abstract: true,
                    parent: 'site.layout',
                    url: "/message",
                    data: {pageTitle: "Mensajería"},
                    views: {
                        'content@site': {
                            templateUrl: MESSAGE_ROOT.layouts + "main.html"
                        }
                    }
                })
                .state('message.inbox', {
                    parent: 'message',
                    url: "/inbox",
                    data: {
                        pageTitle: "Bandeja de entrada",
                        pageHeader: "Bandeja de entrada",
                        pageDescription: "Visualizando (2) mensajes en bandeja de entrada."
                    },
                    views: {
                        '': {
                            templateUrl: MESSAGE_ROOT.views + "inbox.html",
                            controller: "InboxController as vm"
                        }
                    }
                })
                .state('message.notification', {
                    parent: 'message',
                    url: "/notification",
                    data: {
                        pageTitle: "Notificaciones",
                        pageHeader: "Notificaciones",
                        pageDescription: "Visualizando notificaciones."
                    },
                    views: {
                        '': {
                            templateUrl: MESSAGE_ROOT.views + "notification.html",
                            controller: "NotificationController as vm"
                        }
                    }
                })
                //
                //DRIVING TEST ROUTER
                //                
                .state('drivingtest', {
                    abstract: true,
                    parent: 'site.layout',
                    url: "/drivingtest",
                    data: {pageTitle: "Pruebas de Manejo"},
                    views: {
                        'content@site': {
                            templateUrl: DRIVINGTEST_ROOT.layouts + "main.html"
                        }
                    }
                })
                .state('drivingtest.confirmed', {
                    parent: 'drivingtest',
                    url: "/confirmed",
                    data: {
                        pageTitle: "Confirmadas",
                        pageHeader: "Confirmadas",
                        pageDescription: "Visualizando (2) solicitudes."
                    },
                    views: {
                        '': {
                            templateUrl: DRIVINGTEST_ROOT.views + "confirmed.html",
                            controller: "ConfirmedController as vm"
                        }
                    }
                })
                .state('drivingtest.executed', {
                    parent: 'drivingtest',
                    url: "/executed",
                    data: {
                        pageTitle: "Ejecutadas",
                        pageHeader: "Ejecutadas",
                        pageDescription: "Visualizando (2) solicitudes."
                    },
                    views: {
                        '': {
                            templateUrl: DRIVINGTEST_ROOT.views + "executed.html",
                            controller: "ExecutedController as vm"
                        }
                    }
                })
                .state('drivingtest.received', {
                    parent: 'drivingtest',
                    url: "/received",
                    data: {
                        pageTitle: "Recibidas",
                        pageHeader: "Recibidas",
                        pageDescription: "Visualizando (2) solicitudes."
                    },
                    views: {
                        '': {
                            templateUrl: DRIVINGTEST_ROOT.views + "received.html",
                            controller: "ReceivedController as vm"
                        }
                    }
                })
                .state('drivingtest.schedule', {
                    parent: 'drivingtest',
                    url: "/schedule",
                    data: {
                        pageTitle: "Programar",
                        pageHeader: "Programar prueba de manejo.",
                        pageDescription: "Programando nueva prueba de manejo."
                    },
                    views: {
                        'content@site': {
                            templateUrl: DRIVINGTEST_ROOT.views + "schedule.html",
                            controller: "ScheduleController as vm"
                        }
                    }
                });

    }

})();