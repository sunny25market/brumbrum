'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
angular
        .module('app', [
            'ngAnimate',
            'ngCookies',
            'ngMessages',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'ui.router',
            'ui.bootstrap',
            'ngMap',
            'ngTagsInput',
            'LocalStorageModule',
            'ngDialog',
            'angularFileUpload',
            'app.auth',
            'car.module',
            'user.module',
            'message.module',
            'drivingtest.module'
            
        ]);

angular.module('app')
        .config(CompileProviderConfig)
        .constant('APP_UAUTH', {key: 'uAuthBrumBrum'})
        .run(AppRun);

/*@ngInject*/
function AppRun($rootScope, $filter, $state, $document, $location, $log, AuthSvc) {

    $rootScope.$currentDate = new Date();
    $rootScope.$state = $state;
    $rootScope.$location = $location;
    $rootScope.$appGmapref = 'https://maps.googleapis.com/maps/api/js';
    $rootScope.$appUser = AuthSvc.appUser();

    var _allow = [
        'auth.login',
        'auth.logout',
        'auth.reset',
        'auth.resetpassword'
    ];

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {

    });

    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        event.preventDefault();
    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.previousStateName = fromState.name;
        $rootScope.previousStateParams = fromParams;
        $rootScope.currentStateParams = toParams;
        $rootScope.currentState = toState;
        $rootScope.pageTitle = toState.data.pageTitle;
    });

    $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
        console.log(unfoundState.to);
        console.log(unfoundState.toParams);
        console.log(unfoundState.options);
    });

}


/*@ngInject*/
function CompileProviderConfig($compileProvider, ENV) {

    if (ENV === 'dev')
        $compileProvider.debugInfoEnabled(true);
    else
        $compileProvider.debugInfoEnabled(false);
}
