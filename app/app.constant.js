(function() {

    "use strict";

    angular.module('app')
            .constant('VERSION', '1.0.0')
            .constant('ENV', 'dev')
            .constant('API_REST', 'http://xx.xxx.xx./api');
    
})();