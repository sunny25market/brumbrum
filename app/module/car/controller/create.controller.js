(function () {

    "use strict";

    angular.module('car.module')
            .controller('CarCreateController', CarCreateController);

    /*@ngInject*/
    function CarCreateController($scope, $state, CarSvc, ngDialog) {

        var vm = this;

        vm.init = Init;

        vm.fn = {
            deleteImage: function (ref) {
                ngDialog.openConfirm({
                    template: 'app/components/dialog/cmp.delete.html',
                    showClose: true,
                    closeByDocument: true,
                    closeByEscape: true
                });
            },
            editImage: function (ref) {
               console.log(ref);
            },
            addImage: function (ref) {
                ngDialog.openConfirm({
                    template: 'app/components/dialog/cmp.addcarimage.html',
                    showClose: true,
                    closeByDocument: true,
                    closeByEscape: true
                });
            },
        };

        vm.model = {
            color: '#f5f5f5'
        };

        vm.data = {
            categoria: [],
            tipo: [],
            marca: [],
            modelo: [],
            anho: [],
            version: [],
            puesto: [],
            carroceria: [],
            puertas: [],
            transmision: [],
            edicion: [],
            cilindrada: [],
            techo: [],
            combustible: [],
            direccion: [],
            //
            //CARACTERISTICAS...
            caracteristicas: []
        };

        vm.init();

        //Functions

        function Init() {
            CarSvc.caracteristicas().then(function(data){
                vm.data.caracteristicas = data;
            }, function(err){
                alert(err);
            })
        }

        function saveModel(isValid) {
            if (!isValid) {
                alert('Revise los datos del formulario...');
                return;
            }
        }
    }

})();

