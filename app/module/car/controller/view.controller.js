(function () {

    "use strict";

    angular.module('car.module')
            .controller('CarViewController', CarViewController);

    /*@ngInject*/
    function CarViewController($scope, $stateParams) {

        var vm = this;

        vm.init = Init;

        vm.model = {};

        vm.init();

        //Functions

        function Init() {
            loadModel($stateParams.id);
        }

        function loadModel(modelID) {
            if (!modelID)
                return;
        }
    }

})();

