(function() {

    "use strict";

    angular.module('app.auth', []);
    
    angular.module('app.auth')
            .directive('goToLoginLink', goToLoginLink);
    
    /*@ngInject*/
    function goToLoginLink(){
        return {
            restrict:'EA',
            template:function(){
                var template = '';
                template += '<div style="margin-top:15px">';
                template += '<a href="#" data-ui-sref="auth.login">Ir a Inicio de Sesión</a>';
                template += '</div>';
                
                return template;
            }
        }
    }

})();