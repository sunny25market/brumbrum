(function () {

    "use strict";

    angular.module('app.auth')
            .service('AuthSvc', AuthSvc);

    /*@ngInject*/
    function AuthSvc($q, localStorageService, $http, API_REST, APP_UAUTH) {

        var AUTHSERVICEBASE = {
            LOGIN: API_REST + '/auth/login'
        };

        var service = {
            attemptLogin: function (loginObject) {

                return $q(function (resolve, reject) {

                    var login = {
                        username: loginObject.username || "",
                        password: loginObject.password || "",
                        //rememberMe: loginObject.rememberMe || false
                    };

                    var uAuthenticated = {
                        username: login.username,
                        tipoUsuarioDto: {descripcion: "Administrador"},
                        fechaRegistro: "Abr-2017",
                        nombres: "A. Sunny",
                        apellidos: "Market"
                    };

                    resolve(uAuthenticated);
                })
            },
            logout: function () {
                return $q(function (resolve, reject) {
                    localStorageService.clearAll();
                    resolve(true);
                })
            },
            appUser: function () {
                return localStorageService.get(APP_UAUTH.key);
            }
        }

        return service;
    }


})();

