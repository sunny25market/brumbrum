(function () {

    "use strict";

    angular.module('app.auth')
            .controller('RecoverController', RecoverController);

    /*@ngInject*/
    function RecoverController($state, $rootScope, $http, $q, localStorageService, AuthSvc, APP_UAUTH) {

        var vm = this;

        vm.data = {
            password: "",
            password1: ""
        };
        
        vm.init = Init;

        vm.fn = {
            recoverPassword: recoverPassword
        };

        vm.init();

        function Init() {

        }

        function recoverPassword() {
            $state.go('auth.reset');
        }        
    }

})();




