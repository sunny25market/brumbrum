(function () {

    "use strict";

    angular.module('message.module')
            .service('MessageSvc', MessageSvc);

    function MessageSvc($http, $q) {

        var service = {
            query: function(){
                return $q(function(resolve, reject){
                    resolve([]);
                })
            }          
        };

        return service;

    }
})();


