(function () {

    "use strict";

    angular.module('user.module')
            .controller('NotificationController', NotificationController);

    /*@ngInject*/
    function NotificationController($scope, $stateParams, MessageSvc) {

        var vm = this;

        vm.init = Init;

        vm.model = {};

        vm.init();

        //Functions

        function Init() {
            loadModel($stateParams.id);
        }

        function loadModel(modelID) {
            if (!modelID)
                return;
        }
    }

})();

