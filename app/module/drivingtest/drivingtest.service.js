(function () {

    "use strict";

    angular.module('drivingtest.module')
            .service('DrivingtestSvc', DrivingtestSvc);

    function DrivingtestSvc($http, $q) {

        var service = {
            query: function(){
                return $q(function(resolve, reject){
                    resolve([]);
                })
            }          
        };

        return service;

    }
})();


