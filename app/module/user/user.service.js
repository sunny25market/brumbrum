(function () {

    "use strict";

    angular.module('user.module')
            .service('UserSvc', UserSvc);

    function UserSvc($http, $q) {


        var service = {
            create: function (model) {

                function promise(resolve, reject) {
                    resolve({});
                }

                return $q(promise);
            },
            query: function () {

                function promise(resolve, reject) {
                    resolve([]);
                }

                return $q(promise);
            },
            getById: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {
                    if (!modelID)
                        reject("Debe indicar el identificador.");

                    resolve({});
                }

                return $q(promise);
            },
            update: function (model) {

                function promise(resolve, reject) {
                    resolve({});
                }

                return $q(promise);
            },
            delete: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {
                    if (!modelID)
                        reject("Debe indicar el identificador.");

                    resolve(true);
                }

                return $q(promise);
            },
            perfil: function (id) {
                                
                return $q(function (resolve, reject) {
                    $http.get('data/Perfil.json').then(function(response){
                        resolve(response.data);
                    }, function(data){
                        reject(data);
                    });
                })
            }            
        };

        return service;

    }
})();


