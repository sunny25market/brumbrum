(function () {

    "use strict";

    angular.module('user.module')
            .controller('ProfileViewController', ProfileViewController);

    /*@ngInject*/
    function ProfileViewController($scope, $stateParams, UserSvc) {

        var vm = this;

        vm.init = Init;

        vm.model = {};

        vm.init();

        //Functions

        function Init() {
            loadModel($stateParams.id);
        }

        function loadModel(modelID) {
            if (!modelID)
                return;

            UserSvc.perfil().then(function (data) {
                vm.model = data;
            }, function (err) {
                console.log(err);
            });
        }
    }

})();

