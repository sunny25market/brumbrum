(function () {

    "use strict";

    angular.module('user.module')
            .controller('UserViewController', UserViewController);

    /*@ngInject*/
    function UserViewController($scope, $stateParams) {

        var vm = this;

        vm.init = Init;

        vm.model = {};

        vm.init();

        //Functions

        function Init() {
            loadModel($stateParams.id);
        }

        function loadModel(modelID) {
            if (!modelID)
                return;
        }
    }

})();

