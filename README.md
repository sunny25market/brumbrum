BrumProfile Project!
===================


Hola! Si estas viendo esto es porque decidiste colaborar en nuestro proyecto **BrumProfile**.  
No dudes en hacernos sugerencias y sobre todo colaborar con nuestro desarrollo. 

> Lo importante no es lo que sabes, sino lo que logras hacer con ese conocimiento...

> **"Adrian Gomez"**